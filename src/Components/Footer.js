import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div>
                <footer>
                    <span>Created By <a href="#">Nitesh Nepali</a> | <span class="far fa-copyright"></span> 2020 All rights reserved.</span>
                </footer>
            </div>
        )
    }
}
