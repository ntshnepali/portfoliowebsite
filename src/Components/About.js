import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                <section class="about" id="about">
                    <div class="max-width">
                        <h2 class="title">About me </h2>
                        <div class="about-content">
                            <div class="column left">
                                <img src='src/asset/profile.jpeg' alt="" ></img>
                            </div>
                            <div class="column right">
                                <div class="text">I'm Nitesh and I'm a <span class="typing-2">Software Developer</span></div>
                                <p> #............#</p>
                                <a href="#">Download CV</a>
                            </div>
                        </div>
                    </div>
                </section>


            </div>
        )
    }
}
