import React, { Component } from 'react'

export default class Skill extends Component {
    render() {
        return (
            <div>
                <section class="skills" id="skills">
                    <div class="max-width">
                        <h2 class="title">My skills </h2>
                        <div class="skills-content">
                            <div class="column left">
                                <div class="text">My creative skills & Experiences.</div>
                                <p>#.....#</p>
                                <a href="#">Read more</a>
                            </div>
                            <div class="column right">
                                <div class="bars">
                                    <div class="info">
                                        <span>HTML</span>
                                        <span>50%</span>
                                    </div>
                                    <div class="line html"></div>
                                </div>
                                <div class="bars">
                                    <div class="info">
                                        <span>CSS</span>
                                        <span>40%</span>
                                    </div>
                                    <div class="line css"></div>
                                </div>
                                <div class="bars">
                                    <div class="info">
                                        <span>JavaScript</span>
                                        <span>50%</span>
                                    </div>
                                    <div class="line js"></div>
                                </div>
                                <div class="bars">
                                    <div class="info">
                                        <span>PYTHON</span>
                                        <span>50%</span>
                                    </div>
                                    <div class="line python"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}
