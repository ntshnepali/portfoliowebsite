import React, { Component } from 'react'

export default class SocialLink extends Component {


 
    render() {
        return (
            <div class="buttons">
                <div class="row">
                
                    <button class="gitlab" >
                        <span><i class="fab fa-gitlab"></i></span>Gitlab
                        </button>
                        
                    <button class="instagram">
                        <span><i class="fab fa-instagram"></i></span>
                        Instagram</button>
                        
                </div>
                <div class="row">
                    <button class="twitter">
                        <span><i class="fab fa-twitter"></i></span>
                        Twitter</button>
                    <button class="youtube">
                        <span><i class="fab fa-youtube"></i></span>
                        YouTube</button>
                </div>
            </div>


        )
    }
}
