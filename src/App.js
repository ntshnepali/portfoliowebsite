import React from 'react';
import About from './Components/About';
import Header from './Components/Header'
import Home from './Components/Home'
import Skill from './Components/Skill'
import Contact from './Components/Contact'
import Footer from './Components/Footer'
import SocialLink from './Components/SocialLink';
import './Style/Style.css'
function App() {
  return (
    <div>
    <Header/>
    <Home/>
    <About/>

    <Skill/>
    <Contact/>
    <SocialLink />
    <Footer/>
    

    
      
    
    </div>
  );
}

export default App;
